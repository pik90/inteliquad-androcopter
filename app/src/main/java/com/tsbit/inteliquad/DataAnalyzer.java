package com.tsbit.inteliquad;

import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;


public class DataAnalyzer {

    boolean regulatorEnabled;
    public static final float PID_DERIV_SMOOTHING = 0.5f;
    QuadStateReader qState;
    QuadData qData;
    Handler hWnd;

    boolean started = false;
    PIDController yawPID, pitchPID, rollPID;
    PIDData rD,pD,yD;
    //PIDAngle pitchPID, rollPID, yawPID;
    //private float meanThrust, yawAngleTarget, pitchAngleTarget, rollAngleTarget,
    //        altitudeTarget, batteryVoltage, timeWithoutPcRx,
    //        timeWithoutAdkRx;

    //private long previousTime;

    public DataAnalyzer(Handler h, QuadStateReader qstate, QuadData qdata)
    {
        hWnd = h;
        qState = qstate;
        qData = qdata;

        ///OLD PID
        rD = new PIDData();
        pD = new PIDData();
        yD = new PIDData();
        pitchPID = new PIDController(.7f, 0.00f,  0.0f, PIDController.DIRECT, pD);
        rollPID = new PIDController(.7f, 0.00f,  0.0f, PIDController.DIRECT, rD);
        yawPID = new PIDController(1f, 0.05f,  0.3f, PIDController.DIRECT, yD);

        ///NEW PID
        //yawPID = new PIDAngle(3.0f, 0.0f, 0.0f, PID_DERIV_SMOOTHING);
        //pitchPID = new PIDAngle(3.0f, 0.0f, 0.0f, PID_DERIV_SMOOTHING);
        //rollPID = new PIDAngle(3.0f, 0.0f, 0.0f, PID_DERIV_SMOOTHING);
        //yawAngleTarget = 0.0f;
        //pitchAngleTarget = 0.0f;
        //rollAngleTarget = 0.0f;
        //altitudeTarget = 0.0f;
        //regulatorEnabled = false;



    }

    public void Start(){
        qState.resume();
        started = true;

    }

    public void Stop(){
        qState.pause();
        pitchPID.SetMode(PIDController.MANUAL);
        rollPID.SetMode(PIDController.MANUAL);
        yawPID.SetMode(PIDController.MANUAL);
        started = false;
    }

    public MotorOutput Compute(RemoteInput rinput){
        MotorOutput mo = new MotorOutput();
        if(started == false){
            mo.BL = 1000;
            mo.BR = 1000;
            mo.FL = 1000;
            mo.FR = 1000;
            return mo;
        }
        if (rinput.ch3 >= QuadData.MINIMUM_THROTTLE) {
            if(pitchPID.GetMode() == PIDController.MANUAL ||
                    rollPID.GetMode() == PIDController.MANUAL ||
                    yawPID.GetMode() == PIDController.MANUAL){
                pitchPID.SetMode(PIDController.AUTOMATIC);
                rollPID.SetMode(PIDController.AUTOMATIC);
                yawPID.SetMode(PIDController.AUTOMATIC);
                qState.setCurrentStateAsZero();
            }

            if (qState.newData == true) {
                mo.FR = rinput.ch3;
                mo.FL = rinput.ch3;
                mo.BR = rinput.ch3;
                mo.BL = rinput.ch3;

                float currentYaw = (float)qData.yaw;
                float currentPitch = (float)qData.pitch;
                float currentRoll = (float)qData.roll;
                long currentTime = qData.time;

                //float dt = ((float)(currentTime-previousTime)) / 1000000000.0f; // [s].
                //previousTime = currentTime;

                //if(Math.abs(dt) > 1.0) // In case of the counter has wrapped around.
                //    return null;
                // FINE FOR GYRO MODE
                //pD.setpoint = rinput.ch2;pD.input = (float)qData.gyro.x;
                //rD.setpoint = rinput.ch1;rD.input = (float)qData.gyro.y;
                yD.setpoint = rinput.ch4;yD.input = (float)qData.gyro.z;

                // FINE FOR ANGLE MODE
                rD.setpoint = rinput.ch1; rD.input = (float)qData.roll;
                pD.setpoint = rinput.ch2; pD.input = (float)qData.pitch;
                //yD.setpoint = rinput.ch4; yD.input = (float)qData.yaw;


                yawPID.Compute();
                pitchPID.Compute();
                rollPID.Compute();

                SendMessage("PITCH " + pD.output + "\nROLL " + rD.output + "\nYAW " + yD.output+"\nPITCH ANGLE: " + pD.input+"\nROLL ANGLE"+ rD.input + "\nYAW RATE" + yD.input, 0);

                // FINE FOR GYRO MODE
                //mo.FR = rinput.ch3 + (int)pD.output - (int)rD.output + (int)yD.output;
               // mo.FL = rinput.ch3 + (int)pD.output + (int)rD.output - (int)yD.output;
                //mo.BR = rinput.ch3 - (int)pD.output - (int)rD.output - (int)yD.output;
                //mo.BL = rinput.ch3 - (int)pD.output + (int)rD.output + (int)yD.output;

                //FINE FOR ANGLE MODE
                mo.FR = rinput.ch3 - (int)yD.output + (int)pD.output - (int)rD.output;
                mo.FL = rinput.ch3 + (int)yD.output + (int)pD.output + (int)rD.output;
                mo.BR = rinput.ch3 + (int)yD.output - (int)pD.output - (int)rD.output;
                mo.BL = rinput.ch3 - (int)yD.output - (int)pD.output + (int)rD.output;
                //SendMessage(mo.FL + "\t" + mo.FR + "\n" + mo.BL + "\t" + mo.BR, QuadData.JUST_LOG_TO_DISPLAY);
            }
        } else {
            //Log.d("test", rinput.ch1 + "\n" + rinput.ch2 + "\n" + rinput.ch3 + "\n" + rinput.ch4);
            qState.setCurrentStateAsZero();
            if(pitchPID.GetMode() == PIDController.AUTOMATIC ||
                    rollPID.GetMode() == PIDController.AUTOMATIC ||
                    yawPID.GetMode() == PIDController.AUTOMATIC) {
                pitchPID.SetMode(PIDController.MANUAL);
                rollPID.SetMode(PIDController.MANUAL);
                yawPID.SetMode(PIDController.MANUAL);
            }

            mo.BL = 1000;
            mo.BR = 1000;
            mo.FL = 1000;
            mo.FR = 1000;
        }
        return mo;
    }

    /*public MotorOutput Compute(RemoteInput rinput){
        if(!qState.newData)
        {
            //SystemClock.sleep(1);
            return null;
        }
        MotorOutput mo = new MotorOutput();
        float currentYaw = (float)qData.yaw;
        float currentPitch = (float)qData.pitch;
        float currentRoll = (float)qData.roll;
        long currentTime = qData.time;
        float dt = ((float)(currentTime-previousTime)) / 1000000000.0f; // [s].
        previousTime = currentTime;

        if(Math.abs(dt) > 1.0) // In case of the counter has wrapped around.
            return null;

        int yawForce, pitchForce, rollForce, altitudeForce;

        meanThrust = rinput.ch3;
        yawAngleTarget = rinput.ch4;
        rollAngleTarget = rinput.ch1;
        pitchAngleTarget = rinput.ch2;

        if(meanThrust >= 1100)
        {
            // Compute the "forces" needed to move the quadcopter to the
            // set point.
            yawForce = (int)yawPID.getInput(yawAngleTarget, currentYaw, dt);
            pitchForce = (int)pitchPID.getInput(pitchAngleTarget, currentPitch, dt);
            rollForce = (int)rollPID.getInput(rollAngleTarget, currentRoll, dt);

            altitudeForce = (int)meanThrust;

           //SendMessage("YAW:" + yawForce + "\nROLL:" + rollForce + "\nPITCH:" + pitchForce , 0);

            // Compute the power of each motor.
            int tempPowerNW, tempPowerNE, tempPowerSE, tempPowerSW;

            tempPowerNW = altitudeForce; // Vertical "force".
            tempPowerNE = altitudeForce; //
            tempPowerSE = altitudeForce; //
            tempPowerSW = altitudeForce; //

            tempPowerNW += pitchForce; // Pitch "force".
            tempPowerNE += pitchForce; //
            tempPowerSE -= pitchForce; //
            tempPowerSW -= pitchForce; //

            tempPowerNW += rollForce; // Roll "force".
            tempPowerNE -= rollForce; //
            tempPowerSE -= rollForce; //
            tempPowerSW += rollForce; //

            tempPowerNW += yawForce; // Yaw "force".
            tempPowerNE -= yawForce; //
            tempPowerSE += yawForce; //
            tempPowerSW -= yawForce; //

            // Saturate the values, because the motors input are 0-255.
            mo.FL = motorSaturation(tempPowerNW);
            mo.FR = motorSaturation(tempPowerNE);
            mo.BL = motorSaturation(tempPowerSE);
            mo.BR = motorSaturation(tempPowerSW);

            SendMessage(mo.FL + "\t" + mo.FR + "\n" + mo.BL + "\t" + mo.BR, 0);
        }
        else
        {
            mo.FR = 1000;
            mo.FL = 1000;
            mo.BL = 1000;
            mo.BR = 1000;
            yawForce = 0;
            pitchForce = 0;
            rollForce = 0;
            altitudeForce = 0;
            qState.setCurrentStateAsZero();
        }
        return mo;
    }*/


    public void SendMessage(Object obj, int what) {
        Message msg = hWnd.obtainMessage();
        msg.obj = obj;
        msg.what = what;
        hWnd.sendMessage(msg);
    }

    private int motorSaturation(int val)
    {
        if(val > 2000)
            return 2000;
        else if(val < 1000)
            return 1000;
        else
            return val;
    }
}
