package com.tsbit.inteliquad;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.net.InetAddress;

public class NsdClass {
    public static final int SERVICE_FOUND = 100;
    int mPort;
    InetAddress mAddress;
    Context mContext;

    NsdManager nsdManager;
    NsdManager.RegistrationListener mRegistrationListener;
    NsdManager.DiscoveryListener mDiscoveryListener;
    NsdManager.ResolveListener mResolveListener;
    NsdServiceInfo mService;

    Handler mHandler;

    private static String mServiceName = "InteliData";
    public static final String SERVICE_TYPE = "_http._tcp.";
    public static final String TAG = "NSDSERVICE";

    public NsdClass(Context c, Handler h){
        mContext = c;
        nsdManager = (NsdManager) mContext.getSystemService(Context.NSD_SERVICE);
        mHandler = h;
        //registerService(mPort);
    }

    public void SendMessage(Object obj, int what) {
        Message msg = mHandler.obtainMessage();
        msg.obj = obj;
        msg.what = what;
        mHandler.sendMessage(msg);
    }

    public void registerServiceDiscovery(){
        mResolveListener = new NsdManager.ResolveListener() {

            @Override
            public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
                Log.e(TAG, "Resolve failed" + errorCode);
            }

            @Override
            public void onServiceResolved(NsdServiceInfo serviceInfo) {
                Log.e(TAG, "Resolve Succeeded. " + serviceInfo);

                SendMessage(serviceInfo, SERVICE_FOUND);
                nsdManager.stopServiceDiscovery(mDiscoveryListener);
            }
        };

        mDiscoveryListener = new NsdManager.DiscoveryListener() {
            @Override
            public void onDiscoveryStarted(String regType) {
                Log.d(TAG, "Service discovery started");
            }
            @Override
            public void onServiceFound(NsdServiceInfo service) {
                Log.d(TAG, "Service discovery success" + service);
                if(service.getServiceName().equals(mServiceName)){
                    nsdManager.resolveService(service, mResolveListener);

                }
            }
            @Override
            public void onServiceLost(NsdServiceInfo service) {
                Log.e(TAG, "service lost" + service);
            }
            @Override
            public void onDiscoveryStopped(String serviceType) {
                Log.i(TAG, "Discovery stopped: " + serviceType);
            }
            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {
                Log.e(TAG, "Discovery failed: Error code:" + errorCode);
                nsdManager.stopServiceDiscovery(this);
            }
            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {
                Log.e(TAG, "Discovery failed: Error code:" + errorCode);
                nsdManager.stopServiceDiscovery(this);
            }
        };

        nsdManager.discoverServices(SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, mDiscoveryListener);
    }
    public void registerService(int port) {
        mPort = port;
        mService = new NsdServiceInfo();
        mService.setPort(port);
        mService.setServiceName(mServiceName);
        mService.setServiceType(SERVICE_TYPE);

        mRegistrationListener = new NsdManager.RegistrationListener() {
            @Override
            public void onServiceRegistered(NsdServiceInfo NsdServiceInfo) {
                mServiceName = NsdServiceInfo.getServiceName();
                Log.e("Service Test", "Service registered "+mServiceName);
            }
            @Override
            public void onRegistrationFailed(NsdServiceInfo arg0, int arg1) {
            }
            @Override
            public void onServiceUnregistered(NsdServiceInfo arg0) {
            }
            @Override
            public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
            }
        };

        nsdManager.registerService(
                mService, NsdManager.PROTOCOL_DNS_SD, mRegistrationListener);

    }
}
