package com.tsbit.inteliquad;

import android.support.annotation.Nullable;
import android.util.Log;


class PIDData {
    float input, output, setpoint;
}

public class PIDController {


    public static final short AUTOMATIC =	1;
    public static final short MANUAL =0;

    public static final short DIRECT = 0;
    public static final short REVERSE = 1;

    public PIDData pData;
    public long nanos() {return System.nanoTime();}
    public long millis(){return System.currentTimeMillis();}

    float kp, ki, kd;
    float dispKp;				// * we'll hold on to the tuning parameters in user-entered
    float dispKi;				//   format for display purposes
    float dispKd;

    float outputMax, outputMin;

    float ITerm, DTerm;

    boolean inAuto;
    long SampleTime, lastTime;

    short controllerDirection;
    boolean angular = false;

    public PIDController(float Kp, float Ki, float Kd, short ControllerDirection, PIDData data)
    {
        inAuto = false;

        pData = data;

        SetOutputLimits(-300, 300);

        SampleTime = 4;							//default Controller Sample Time is 0.1 0.001 seconds

        SetControllerDirection(ControllerDirection);
        SetTunings(Kp, Ki, Kd);

        lastTime = millis()-SampleTime;
    }

    public void SetAngular(boolean ang){
        angular = ang;
    }

    public void SetOutputLimits(float Min, float Max)
    {
        if(Min >= Max) return;
        outputMin = Min;
        outputMax = Max;

        if(inAuto)
        {
            if(pData.output > outputMax) pData.output = outputMax;
            else if(pData.output < outputMin) pData.output = outputMin;

            if(ITerm > outputMax) ITerm= outputMax;
            else if(ITerm < outputMin) ITerm= outputMin;
        }
    }

    public void SetMode(int Mode)
    {
        boolean newAuto = (Mode == AUTOMATIC);
        if(newAuto == !inAuto)
        {  /*we just went from manual to auto*/
            Initialize();
        }
        inAuto = newAuto;
    }

    public void SetControllerDirection(short Direction)
    {
        if(inAuto && Direction !=controllerDirection)
        {
            kp = (0 - kp);
            ki = (0 - ki);
            kd = (0 - kd);
        }
        controllerDirection = Direction;
    }

    private void Initialize()
    {
        ITerm = 0;
        DTerm = 0;
        //ITerm = pData.output;
        //DTerm = pData.input;
        if(ITerm > outputMax) ITerm = outputMax;
        else if(ITerm < outputMin) ITerm = outputMin;
    }

    public boolean Compute()
    {
        //Log.d("DTERM IS: ", DTerm + "");



        if(!inAuto) return false;
        long now = millis();
        long timeChange = (now - lastTime);
        if(timeChange>=SampleTime)
        {
      /*Compute all the working error variables*/
            float input = pData.input;
            float error = pData.setpoint - input;
            ITerm+= (ki * error);
            if(ITerm > outputMax) ITerm= outputMax;
            else if(ITerm < outputMin) ITerm= outputMin;
            float dInput = (input - DTerm);

            /*if(angular){
                float rawDifference = pData.setpoint - pData.input;
                float difference = (float)QuadStateReader.getMainAngle(rawDifference);
                boolean differenceJump = (difference != rawDifference);
                if(differenceJump){
                    dInput = 0.0f;
                }
            }*/


      /*Compute PID Output*/
            float output = kp * error + ITerm- kd * dInput;

            if(output > outputMax) output = outputMax;
            else if(output < outputMin) output = outputMin;
            pData.output = output;

      /*Remember some variables for next time*/
            DTerm = input;
            lastTime = now;
            return true;
        }
        else return false;
    }

    public void SetTunings(float  p, float i, float d)
    {
        if (p<0 || i<0 || d<0) return;

        dispKp = p; dispKi = i; dispKd = d;

        float SampleTimeInSec = ((float)SampleTime);///1000;

        kp = p;
        ki = i * SampleTimeInSec;
        kd = d / SampleTimeInSec;

        if(controllerDirection == REVERSE)
        {
            kp = (0 - kp);
            ki = (0 - ki);
            kd = (0 - kd);
        }
    }

    void SetSampleTime(int NewSampleTime)
    {
        if (NewSampleTime > 0)
        {
            double ratio  = (double)NewSampleTime
                    / (double)SampleTime;
            ki *= ratio;
            kd /= ratio;
            SampleTime = (long)NewSampleTime;
        }
    }



    double GetKp(){ return  dispKp; }
    double GetKi(){ return  dispKi;}
    double GetKd(){ return  dispKd;}
    int GetMode(){ return  inAuto ? AUTOMATIC : MANUAL;}
    int GetDirection(){ return controllerDirection;}



}
