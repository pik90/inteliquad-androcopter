package com.tsbit.inteliquad;


class RemoteInput{
    public int ch1, ch2, ch3, ch4, ch5, ch6;/*canalele telecomenzii
    ch1 = ROLL
    ch2 = PITCH
    ch3 = THROTTLE
    ch4 = YAW
    ch5 = AUX1
    ch6 = AUX2
    */
    public RemoteInput(){}
}

class MotorOutput{
    public int FL, FR, BR, BL;
}

class GyroOutput{
    public double x,y,z;
}

public class QuadData {
    public static final int NO_USB_FOUND_ERROR = 404;
    public static final int JUST_LOG_TO_DISPLAY = 0;
    public static final int START_RECORDING = 300;
    public static  final int SET_PID_TUNINGS = 1;

    public static final double PI = (Math.PI);
    public static final double RAD_TO_DEG = 180.0f / PI;


    public static final int MINIMUM_THROTTLE = 1050;

    public double yaw, pitch, roll, // [degrees].
            baroElevation, gpsElevation; // [m].
    public long time; // [nanoseconds].
    public double longitude, latitude; // [degrees].
    public float gpsAccuracy, xPos, yPos; // [m].
    public float xSpeed, ySpeed; // [m/s].
    public int nSatellites, gpsStatus; // [].
    GyroOutput gyro; // [degrees/s].

    public final static int[]  centerPos = {1512, 1536, 0, 1520};

}


