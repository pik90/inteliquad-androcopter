package com.tsbit.inteliquad;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.hardware.SensorManager;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;

import java.util.List;


public class UserFor extends AppCompatActivity {
    QuadData qData;
    Handler mHandler;
    TextView mLogView;
    Button bToggleState;
    UsbConnection talkBridge;

    UsbManager manager = null;

    QuadStateReader qState = null;

    boolean bRunning = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_for);
        init();
    }
    private void init(){
        qData = new QuadData();
        qData.gyro = new GyroOutput();
        bToggleState = (Button) findViewById(R.id.startstop);
        mLogView = (TextView) findViewById(R.id.LogView);
        bToggleState.setOnClickListener(ToggleListener);
        mHandler = h;
        qState = new QuadStateReader(mHandler, (SensorManager) getSystemService(Context.SENSOR_SERVICE), qData);

    }

    private class UsbDriverInfo{
        boolean empty;
        UsbSerialDriver driver;
    }

    private UsbDriverInfo GetDriver() {
        manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        List<UsbSerialDriver> availableDrivers = UsbSerialProber.getDefaultProber().findAllDrivers(manager);
        UsbDriverInfo data = new UsbDriverInfo();
        data.empty = false;
        if (availableDrivers.isEmpty()) {
            ShowLog("EROARE!\nNu a fost gasit niciun dispozitiv USB.");
            data.empty = true;
            return data;
        }

        data.driver = availableDrivers.get(0);
        return data;
    }

    private void ShowLog(String t){
        mLogView.setText(t);
    }

    private View.OnClickListener ToggleListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(!bRunning){
                /*PIDData pd = new PIDData();
                PIDController pid = new PIDController(0.9f, 0.05f,  0.08f, PIDController.DIRECT, pd);
                pd.setpoint = 0;
                pid.SetOutputLimits(-300, 300);
                pid.SetMode(PIDController.AUTOMATIC);
                int t=97,t2;
                for(int i=0;i<200;i++){
                    pd.input = t; pid.Compute();
                    t2 = (int)pd.output;
                    Log.d("PID:", t + "\t" + t2);
                    t += t2*1.65;
                    try {
                        Thread.sleep(4);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }*/



                // Telefonul nu comunica cu placa. Comunicarea trebuie pornita
                /*UsbDriverInfo data = GetDriver();
                if(data.empty == false) {
                    talkBridge = new UsbConnection(manager, data.driver, mHandler, qData, qState);
                    talkBridge.start();
                    bToggleState.setText("OPRIRE COMUNICARE");
                    bRunning = true;
                }*/
            }else{
                bToggleState.setText("INITIALIZARE COMUNICARE");
                talkBridge.StopRunning();
                talkBridge = null;
                bRunning = false;
            }
        }
    };


    Handler h = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            switch(msg.what){
                case QuadData.JUST_LOG_TO_DISPLAY:


                    //mLogView.append(rollPID2.Compute(qData.gyro.z, 0) + "");
                    mLogView.setText(msg.obj.toString());
                    break;

                //case QuadData.COMPUTATION_DONE:

            }
        }
    };
}
