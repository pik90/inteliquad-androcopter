package com.tsbit.inteliquad;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;



public class QuadStateReader{
    SensorManager sManager;
    float[] rotationMatrix;
    float[] yawPitchRollVec;
    float[] rotVec;
    float[] gyroVec;
    float[] my90DegRotationMatrix = {1, 0, 0,
                                     0, 0, 1,
                                     0,-1, 0};

    float[] zeroMatrix;
    float pitchZero = 0f, rollZero = 0f, yawZero = 0f;

    Sensor sRot;
    Sensor sBaro;
    Sensor sGyro;
    boolean enabled = false;
    boolean newData;

    QuadData qData;
    Handler hWnd;

    public QuadStateReader(Handler h, SensorManager SM, QuadData q){
        sManager = SM;
        sGyro = sManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        sBaro = sManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        sRot = sManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);

        if ((sRot == null)) {
            Log.e("DroneIntel", "Lipeste senzorul de rotatie!");
        }
        if ((sBaro == null)) {
            Log.e("DroneIntel", "Lipeste barometrul!");
        }
        if ((sGyro == null)) {
            Log.e("DroneIntel", "Lipeste giroscopul!");
        }

        yawPitchRollVec = new float[3];
        rotVec = new float[3];
        gyroVec = new float[3];
        rotationMatrix = new float[9];
        zeroMatrix = new float[9];
        qData = q;

        enabled = true;
        newData = false;
        hWnd = h;
    }

    public void pause(){
        if(sRot != null)sManager.unregisterListener(rotListener);
        if(sBaro != null)sManager.unregisterListener(baroListener);
        if(sGyro != null)sManager.unregisterListener(gyroListener);
    }

    public void resume() {
        if(sRot != null)sManager.registerListener(rotListener, sRot, SensorManager.SENSOR_DELAY_FASTEST);
        if(sBaro != null)sManager.registerListener(baroListener, sBaro, SensorManager.SENSOR_DELAY_FASTEST);
        if(sGyro != null)sManager.registerListener(gyroListener, sGyro, SensorManager.SENSOR_DELAY_FASTEST);
    }

    public void setZeroPos(float pitch, float roll, float yaw){
        pitchZero = pitch;
        rollZero = roll;
        yawZero = yaw;
    }

    boolean setZero = true;
    public void setCurrentStateAsZero()
    {
        setZero = true;
        yawZero = yawPitchRollVec[0];
        pitchZero = yawPitchRollVec[1];
        rollZero = yawPitchRollVec[2];

    }

    SensorEventListener rotListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            // Get the time and the rotation vector.
            qData.time = event.timestamp;
            System.arraycopy(event.values, 0, rotVec, 0, 3);

            // Calculare "YAW" , "PITCH" si "ROLL"
            SensorManager.getRotationMatrixFromVector(rotationMatrix, rotVec);
            //if(setZero == true){
            //    setZero = false;
            //    System.arraycopy(rotationMatrix, 0, zeroMatrix, 0,9);
            //}

            rotationMatrix = matrixMultiplication(rotationMatrix, my90DegRotationMatrix);

            //SensorManager.getAngleChange(yawPitchRollVec, rotationMatrix, zeroMatrix);
            SensorManager.getOrientation(rotationMatrix, yawPitchRollVec);

            // Make the measurements relative to the user-defined zero orientation.
            qData.yaw = getMainAngle(-(yawPitchRollVec[0]-yawZero) * QuadData.RAD_TO_DEG);
            //if(qData.yaw > -3 && qData.yaw < 3)  qData.yaw = 0;
            qData.pitch = getMainAngle(-(yawPitchRollVec[1]-pitchZero) * QuadData.RAD_TO_DEG);
            //if(qData.pitch > -3 && qData.pitch < 3)  qData.pitch = 0;
            qData.roll = getMainAngle((yawPitchRollVec[2]-rollZero) * QuadData.RAD_TO_DEG);
            //if(qData.roll > -3 && qData.roll < 3)  qData.roll = 0;

            //qData.yaw = qData.yaw + qData.roll;
            //qData.yaw = yawPitchRollVec[0] * qData.RAD_TO_DEG;
            //qData.pitch = yawPitchRollVec[1]* qData.RAD_TO_DEG;
            //qData.roll = yawPitchRollVec[2]* qData.RAD_TO_DEG;
            newData = true;
            //SendMessage("PITCH:\n"+ qData.pitch +"\nROLL:\n" + qData.roll + "\nYAW:\n"+qData.yaw, 0);


        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    private float[] matrixMultiplication(float[] a, float[] b) {
        float[] r = new float[9];
        r[0] = a[0] * b[0] + a[1] * b[3] + a[2] * b[6];
        r[1] = a[0] * b[1] + a[1] * b[4] + a[2] * b[7];
        r[2] = a[0] * b[2] + a[1] * b[5] + a[2] * b[8];

        r[3] = a[3] * b[0] + a[4] * b[3] + a[5] * b[6];
        r[4] = a[3] * b[1] + a[4] * b[4] + a[5] * b[7];
        r[5] = a[3] * b[2] + a[4] * b[5] + a[5] * b[8];

        r[6] = a[6] * b[0] + a[7] * b[3] + a[8] * b[6];
        r[7] = a[6] * b[1] + a[7] * b[4] + a[8] * b[7];
        r[8] = a[6] * b[2] + a[7] * b[5] + a[8] * b[8];

        return r;
    }

    public void SendMessage(Object obj, int what) {
        Message msg = hWnd.obtainMessage();
        msg.obj = obj;
        msg.what = what;
        hWnd.sendMessage(msg);
    }

    SensorEventListener baroListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            float rawAltitudeUnsmoothed = SensorManager.getAltitude(SensorManager.PRESSURE_STANDARD_ATMOSPHERE, event.values[0]);
            qData.baroElevation = rawAltitudeUnsmoothed;
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };
    SensorEventListener gyroListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {

            qData.time = event.timestamp;
            qData.gyro.x = event.values[0] * QuadData.RAD_TO_DEG;
            if(qData.gyro.x > -4 && qData.gyro.x < 4)  qData.gyro.x = 0;
            qData.gyro.y = event.values[1] * QuadData.RAD_TO_DEG;
            if(qData.gyro.y > -4 && qData.gyro.y < 4)  qData.gyro.y = 0;
            qData.gyro.z = event.values[2] * QuadData.RAD_TO_DEG;
            if(qData.gyro.z > -4 && qData.gyro.z < 4)  qData.gyro.z = 0;


            newData = true;
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    public static double getMainAngle(double angle)
    {
        while(angle < -180.0f)
            angle += 360.0f;
        while(angle > 180.0f)
            angle -= 360.0f;

        return angle;
    }
}
