package com.tsbit.inteliquad;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class BluetoothConnection {
    public static final java.util.UUID MY_UUID = java.util.UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    boolean bRunning;

    Handler hWnd;
    QuadData qData;
    QuadStateReader qState;
    BluetoothSocket btSocket;
    DataAnalyzer processor;

    BluetoothAdapter btAdapter;
    Thread t;

    public BluetoothConnection(Handler h, QuadData q, QuadStateReader qs, BluetoothDevice b, BluetoothAdapter BA){
        qData = q;
        hWnd = h;
        qState = qs;
        processor = new DataAnalyzer(hWnd, qState, qData);
        BluetoothSocket tmp = null;
        try {
            // MY_UUID is the app's UUID string, also used by the server code
            tmp = b.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException e) {
            e.printStackTrace();

        }
        btSocket = tmp;
        btAdapter = BA;
    }
    public long millis(){return System.currentTimeMillis();}

    public void start(){
        t = new Thread(r);
        t.start();
    }
    public boolean isAlive(){
        return t.isAlive();
    }
    Runnable r = new Runnable() {
        @Override
        public void run() {
            btAdapter.cancelDiscovery();
            try {
                btSocket.connect();
            } catch (IOException e) {
                e.printStackTrace();
                try {
                    btSocket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            bRunning = true;
            DataInputStream inputStream = null;
            DataOutputStream outputStream = null;
            RemoteInput rinput = new RemoteInput();
            MotorOutput mo;
            boolean read = false;
            try {
                inputStream = new DataInputStream(btSocket.getInputStream());
                outputStream = new DataOutputStream(btSocket.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
            processor.Start();

            long time = millis();


            while(bRunning){
                try {
                    byte[] t= new byte[inputStream.available()];
                    time = millis();

                    inputStream.read(t);

                    for(int i=0;i<t.length; i++){
                        bb by = new bb(t[i]);
                        bytes.add(by);
                    }
                    int tmp;
                    while (bytes.size() >= 9) {
                        read = false;
                        tmp = (getb()) & 0xFF;

                        while ((tmp != 251 && bytes.size() > 0)) {
                            tmp = (getb()) & 0xFF;
                        }

                        if (bytes.size() >= 8) {
                            rinput.ch1 = (((getb() & 0xFF) * 250 + (getb() & 0xFF) - QuadData.centerPos[0])) / 5;
                            rinput.ch2 = (((getb() & 0xFF) * 250 + (getb() & 0xFF) - QuadData.centerPos[1])) / 5;
                            rinput.ch3 = ((getb() & 0xFF) * 250 + (getb() & 0xFF));
                            rinput.ch4 = (((getb() & 0xFF) * 250 + (getb() & 0xFF) - QuadData.centerPos[3]) / 5);

                            if(rinput.ch1 < 3 && rinput.ch1 > -3) rinput.ch1 = 0;
                            if(rinput.ch2 < 3 && rinput.ch2 > -3) rinput.ch2 = 0;
                            if(rinput.ch4 < 3 && rinput.ch4 > -3) rinput.ch4 = 0;
                            if(rinput.ch3 > 1000) read = true;
                        }

                    }

                    if(read){
                        //processor.SendMessage(rinput.ch1 + "\n" + rinput.ch2 + "\n" + rinput.ch3 + "\n" + rinput.ch4, 0);
                        mo = processor.Compute(rinput);
                        if(mo != null) writeFormat(outputStream, mo);
                    }

                } catch (IOException e) {
                    e.printStackTrace();

                }



            }
            processor.Stop();
        }
    };

    private boolean writeFormat(DataOutputStream out, MotorOutput moutput) {

        //SendMessage(moutput.FL + "\t" + moutput.FR + "\n" + moutput.BL + "\t" + moutput.BR, QuadData.JUST_LOG_TO_DISPLAY);
        byte[] w = new byte[9];
        w[0] = (byte) 251;
        w[1] = (byte) (moutput.FL / 250);
        w[2] = (byte) (moutput.FL % 250);

        w[3] = (byte) (moutput.FR / 250);
        w[4] = (byte) (moutput.FR % 250);

        w[5] = (byte) (moutput.BR / 250);
        w[6] = (byte) (moutput.BR % 250);

        w[7] = (byte) (moutput.BL / 250);
        w[8] = (byte) (moutput.BL % 250);

        try {
            out.write(w);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void close(){
        bRunning = false;
        try {
            btSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public byte getb() {
        byte b = bytes.get(0).b;
        bytes.remove(0);
        return (b);
    }
    ArrayList<bb> bytes = new ArrayList<>();
    class bb{
        byte b;

        public bb(byte by){
            b = by;
        }
    }
}
