package com.tsbit.inteliquad;

import android.animation.Animator;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.hardware.Camera;
import android.hardware.SensorManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

import static android.bluetooth.BluetoothAdapter.*;

public class BluetoothActivity extends AppCompatActivity {
    private static int REQUEST_START = 0;

    QuadData qData;
    QuadStateReader qState;

    private BluetoothAdapter BA;
    private Set<BluetoothDevice> pairedDevices;
    private ArrayList<BluetoothDevice> devices;
    FloatingActionButton fab;
    ListView listView;
    TextView tv;
    TextView tv2;
    FrameLayout CameraFrame;
    boolean btState;
    BluetoothDevice targetDevice;
    RevealLayout talkUi;
    BluetoothActivity activity = this;

    NsdClass nsdClass = null;
    Client mLanClient = null;
    ArrayAdapter foundDevices;

    BluetoothConnection bConn = null;

    private Camera mCamera;
    private CameraPreview mPreview=null;

    @Override
    protected void onDestroy(){

        super.onDestroy();
        if(mPreview != null) {
            mPreview.stop();
        }
        unregisterReceiver(mReceiver);
        if(!isChangingConfigurations())
            if(!btState) off();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        if(mPreview != null){
            mPreview.stop();
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        init();
    }
    Button b ;
    private void init(){

        BA = getDefaultAdapter();

        //Initiating UI elements.
        fab = (FloatingActionButton) findViewById(R.id.fab);
        tv = (TextView) findViewById(R.id.logView);
        tv2 = (TextView) findViewById(R.id.textView2);
        talkUi = (RevealLayout) findViewById(R.id.reveal_layout);

        //Initiating processing classes
        qData = new QuadData();
        qData.gyro = new GyroOutput();
        qState = new QuadStateReader(h, (SensorManager) getSystemService(Context.SENSOR_SERVICE), qData);
        qState.resume();

        //qState.setCurrentStateAsZero();

        // Create our Camera Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(activity);
        CameraFrame =   (FrameLayout) findViewById(R.id.frame);
        CameraFrame.addView(mPreview);

        //When we press the play button to start the BluetoothConnection
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bConn == null) {
                    if (targetDevice == null) {
                       ShowMessage("Alege dispozitivul bluetooth atasat dronei.");
                    } else {
                        ShowMessage("Procesul a fost pornit.");

                        bConn = new BluetoothConnection(h, qData, qState, targetDevice, BA);
                        bConn.start();
                        fab.setImageResource(R.drawable.ic_pause_black_24dp);
                        int cx = (fab.getLeft() + fab.getRight()) / 2;
                        int cy = (fab.getTop() + fab.getBottom()) / 2;

                        talkUi.setVisibility(View.VISIBLE);
                        talkUi.show(cx, cy, 1400);
                        h.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                CameraFrame.setVisibility(View.VISIBLE);
                                //CameraPreview.prepareVideoRecorder
                            }
                        }, 500);
                    }
                }else{
                    if(bConn.isAlive()){
                        bConn.close();
                        bConn = null;
                        Snackbar.make(view, "Procesul a fost oprit.", Snackbar.LENGTH_SHORT)
                                .setAction("Action", null).show();
                    }
                }



                //mCollapseFabButton.animate().rotationBy(135).setDuration(250).start();
            }
        });
        foundDevices = new ArrayAdapter(this, android.R.layout.simple_list_item_1);
        devices = new ArrayList<>();
        initList();
        btState = BA.isEnabled();

        if(!btState)
            on();
        else{
            pairedDevices = BA.getBondedDevices();
            IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            registerReceiver(mReceiver, filter); // Don't forget to unregister during onDestroy
            BA.startDiscovery();
        }

        if(nsdClass == null){
            nsdClass = new NsdClass(getBaseContext(), h);
            nsdClass.registerServiceDiscovery();
        }
    }


    public void on(){
        if (!BA.isEnabled()) {
            Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOn, REQUEST_START);
        }
    }

    public void off(){
        BA.disable();
    }

    private void ShowMessage(String msg){
        Snackbar.make(fab, msg, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == REQUEST_START) {
            if (resultCode == RESULT_OK) {
                pairedDevices = BA.getBondedDevices();
                ShowMessage("Turned on");
                IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                registerReceiver(mReceiver, filter); // Don't forget to unregister during onDestroy
                BA.startDiscovery();
            }else{
                ShowMessage("Bluetoothul este necesar rularii acestei aplicatii.");
            }
        }
    }


    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {

                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // Add the name and address to an array adapter to show in a ListView
                if(pairedDevices.contains(device)){
                    foundDevices.insert(device.getName() + " (PAIRED) \n" + device.getAddress(), 0);
                    devices.add(0, device);
                }else{
                    foundDevices.add(device.getName() + "\n" + device.getAddress());
                    devices.add(device);
                }

            }
        }
    };


    private void initList(){
        listView = (ListView)findViewById(R.id.listView);
        listView.setAdapter(foundDevices);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,long arg3) {
                BA.cancelDiscovery();
                targetDevice = devices.get(position);
            }
        });
    }

    Handler h = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            switch(msg.what){
                case QuadData.JUST_LOG_TO_DISPLAY:
                    tv.setText(msg.obj.toString());
                   // tv2.setText(msg.obj.toString());
                    //Log.e("POWERS", msg.obj.toString());
                    break;
                case QuadData.START_RECORDING:
                    break;

                case NsdClass.SERVICE_FOUND:
                    NsdServiceInfo tmp = (NsdServiceInfo) msg.obj;
                    mLanClient = new Client(tmp.getHost(), tmp.getPort(), h);
                    Log.e("SERVICE FOUND!", tmp.getServiceName() + " on port "+tmp.getPort());
                    break;
                case QuadData.SET_PID_TUNINGS:
                    try {
                        JSONObject js = new JSONObject(msg.obj.toString());
                        if(bConn != null) {
                            ShowMessage("Setting new rates");
                            bConn.processor.pitchPID.SetTunings((float) js.getInt("PKP") / 100, (float) js.getInt("PKI") / 100, (float) js.getInt("PKD") / 100);
                            bConn.processor.rollPID.SetTunings((float) js.getInt("RKP") / 100, (float) js.getInt("RKI") / 100, (float) js.getInt("RKD") / 100);
                            bConn.processor.yawPID.SetTunings((float) js.getInt("YKP") / 100, (float) js.getInt("YKI") / 100, (float) js.getInt("YKD") / 100);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
            }
        }
    };

}
