package com.tsbit.inteliquad;

import android.annotation.TargetApi;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.util.SerialInputOutputManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class UsbConnection extends Thread {
    UsbSerialDriver mDriver;
    UsbManager manager;
    Handler hWnd;
    DataAnalyzer dataAnalyzer;

    ArrayList<bb> bytes = new ArrayList<>();

    private final ExecutorService mExecutor = Executors.newSingleThreadExecutor();
    private SerialInputOutputManager mSerialIoManager;

    public void SendMessage(Object obj, int what) {
        Message msg = hWnd.obtainMessage();
        msg.obj = obj;
        msg.what = what;
        hWnd.sendMessage(msg);
    }

    UsbSerialPort port;

    class bb {
        byte b;
    }

    public UsbConnection(UsbManager mngr, UsbSerialDriver driver, Handler hnd, QuadData q, QuadStateReader qstate) {
        manager = mngr;
        mDriver = driver;
        hWnd = hnd;

        UsbDeviceConnection connection = manager.openDevice(driver.getDevice());

        if (connection == null) {
            hWnd.sendEmptyMessage(QuadData.NO_USB_FOUND_ERROR);
            return;
        }
        port = driver.getPorts().get(0);
        try {
            port.open(connection);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            port.setParameters(115200, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);
        } catch (IOException e) {
            SendMessage("A aparut o eroare neasteptata.", QuadData.NO_USB_FOUND_ERROR);
        }

        dataAnalyzer = new DataAnalyzer(hnd, qstate, q);
    }

    public byte getb() {
        byte b = bytes.get(0).b;
        bytes.remove(0);
        return (b);
    }

    public void StopRunning() {
        dataAnalyzer.Stop();
        mExecutor.shutdown();
        mSerialIoManager.stop();
    }

    @Override
    public void run() {
        dataAnalyzer.Start();
        mSerialIoManager = new SerialInputOutputManager(port, mListener);
        mExecutor.submit(mSerialIoManager);

    }

    private boolean writeFormat(MotorOutput moutput) {

        SendMessage(moutput.FL + "\t" + moutput.FR + "\n" + moutput.BL + "\t" + moutput.BR, QuadData.JUST_LOG_TO_DISPLAY);
        byte[] w = new byte[9];
        w[0] = (byte) 251;
        w[1] = (byte) (moutput.FR / 250);
        w[2] = (byte) (moutput.FR % 250);

        w[3] = (byte) (moutput.FL / 250);
        w[4] = (byte) (moutput.FL % 250);

        w[5] = (byte) (moutput.BR / 250);
        w[6] = (byte) (moutput.BR % 250);

        w[7] = (byte) (moutput.BL / 250);
        w[8] = (byte) (moutput.BL % 250);

        try {
            port.write(w, 2);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }



    private final SerialInputOutputManager.Listener mListener =
            new SerialInputOutputManager.Listener() {

                @Override
                public void onRunError(Exception e) {
                    SendMessage("A aparut o eroare la citire.", QuadData.NO_USB_FOUND_ERROR);
                }

                @Override
                public void onNewData(final byte[] data) {
                    for (int i = 0; i < data.length; i++) {
                        bb tmp = new bb();
                        tmp.b = data[i];
                        bytes.add(tmp);
                    }

                    int t;
                    boolean read = false;
                    RemoteInput rinput;
                    MotorOutput mo;

                    rinput = new RemoteInput();
                    //SendMessage("running " + bytes.size(), QuadData.JUST_LOG_TO_DISPLAY);
                    read = false;

                    while (bytes.size() >= 9) {
                        read = false;
                        t = (getb()) & 0xFF;

                        while ((t != 251 && bytes.size() > 0)) {
                            t = (getb()) & 0xFF;
                        }

                        if (bytes.size() >= 8) {
                            rinput.ch1 = (((getb() & 0xFF) * 250 + (getb() & 0xFF) - QuadData.centerPos[0])) / 3;
                            rinput.ch2 = (((getb() & 0xFF) * 250 + (getb() & 0xFF) - QuadData.centerPos[1])) / 3;
                            rinput.ch3 = ((getb() & 0xFF) * 250 + (getb() & 0xFF));
                            rinput.ch4 = (((getb() & 0xFF) * 250 + (getb() & 0xFF) - QuadData.centerPos[3]) / 3);

                            if(rinput.ch1 < 3 && rinput.ch1 > -3) rinput.ch1 = 0;
                            if(rinput.ch2 < 3 && rinput.ch2 > -3) rinput.ch2 = 0;
                            if(rinput.ch4 < 3 && rinput.ch4 > -3) rinput.ch4 = 0;
                            read = true;
                        }



                    }

                    if (read) {
                        mo = dataAnalyzer.Compute(rinput);
                        writeFormat(mo);
                    }

                }
            };
}


