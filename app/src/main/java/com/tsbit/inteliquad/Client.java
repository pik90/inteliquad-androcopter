package com.tsbit.inteliquad;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;


public class Client {
    public static final int LOG_DATA = 0;
    public static final int SERVER_PORT = 1;
    public static final int SERVER_ADDRESS = 2;
    Handler mHandler;
    ComClass comClass;
    int mPort;
    InetAddress mAddress;
    Context mContext;
    public Client(InetAddress addr, int port, Handler hnd){
        mHandler = hnd;
        mPort = port;
        mAddress = addr;

        comClass = new ComClass(mHandler);
        Thread clientThread = new Thread(comClass);
        clientThread.start();
    }

    class ComClass implements Runnable{
        Socket mSocket;
        Handler mHandler;

        private final static String TAG = "SERVERSOCKET";

        public ComClass(Handler hnd) {
            Log.d(TAG, "ServerClass created");
            mHandler = hnd;
        }

        @Override
        public void run() {
            try {
                mSocket = new Socket(mAddress, mPort);
            } catch (IOException e) {
                e.printStackTrace();
            }

            DataInputStream input = null;
            DataOutputStream output = null;
            try {
                input = new DataInputStream(mSocket.getInputStream());
                output = new DataOutputStream(mSocket.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
            while (mSocket.isConnected()) {
                try {
                    if(input.available() > 0){
                        Log.e(TAG, "Data here " + input.available());
                        String s = input.readUTF();
                        SendMessage(s, QuadData.SET_PID_TUNINGS);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        public void SendMessage(Object obj, int what) {
            Message msg = mHandler.obtainMessage();
            msg.obj = obj;
            msg.what = what;
            mHandler.sendMessage(msg);
        }
    }
}
